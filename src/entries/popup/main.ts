import "./style.css";

const buttonElement = document.querySelector("button")!;

chrome.runtime.onMessage.addListener(async function (message) {
  switch (message.type) {
    case 'getUserFiltersResponse':
      buildUserFiltersList(message.currentUserFilters)
  }
});

type Filter = {
  text: string,
  enabled: boolean | null,
  slow: boolean,
  type: string,
  thirdParty: boolean | null,
  selector: string | null,
  csp: string | null
}

function buildUserFiltersList(currentUserFilters: [Filter]) {
  (<HTMLElement>document.getElementById("current-filters")).innerHTML = "";
  for (const filter of currentUserFilters){
    const listItem = <HTMLDivElement>(document.createElement('div'));
    listItem.innerText = filter.text;
    
    const removeButton = <HTMLButtonElement>document.createElement('button');
    removeButton.innerText = "X";
    removeButton.onclick = () => removeFilter(filter.text);
    listItem.append(removeButton);

    (<HTMLElement>document.getElementById("current-filters")).appendChild(listItem);
  }
}

function addFilter(filterValue: string) {
  chrome.runtime.sendMessage({ type: 'addFilter', filter: filterValue });
}

async function removeFilter(filterValue: string) {
  await chrome.runtime.sendMessage({ type: 'removeFilter', filter: filterValue });
  await requestCurrentFilters();
}

async function requestCurrentFilters() {
  const result = await chrome.runtime.sendMessage({ type: 'getUserFilters' });
  return result;
}

buttonElement.addEventListener("click", async () => {
  const filterInput = (<HTMLInputElement>document.getElementById('filterText'));
  const filterText = filterInput.value;

  try {
    addFilter(filterText);
    await requestCurrentFilters();
  } catch (err) {
    alert(err);
  }
});

// show the filters on page load
requestCurrentFilters();