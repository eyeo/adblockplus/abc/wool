import browser from "webextension-polyfill";
import EWE from "@eyeo/webext-ad-filtering-solution";

browser.runtime.onInstalled.addListener(() => {
  console.log("Extension installed");
});

type Recommendation = {
  id: string,
  languages: Array<string>,
  title: string,
  requires: Array<string>,
  includes: Array<string>,
  type: string,
  url: string,
  mv2URL?: string
};

type EWEAddon = {
  name: string,
  version: string,
  bundledSubscriptions: Array<Recommendation>,
  bundledSubscriptionsPath?: string,
  telemetry?: { bearer?: string, url: string }
}

(async () => {
  const testRecommendation: Recommendation = {
    id: '1',
    languages: [],
    title: '',
    requires: [],
    includes: [],
    type: '',
    url: ''
  };

  const addon: EWEAddon = {
    name: "Wool",
    version: "1",
    bundledSubscriptions: [testRecommendation],
    bundledSubscriptionsPath: './',
    telemetry: {
      bearer: '',
      url: ''
    }
  }
  EWE.telemetry.onError.addListener((e: any) => {
    console.error(e.message);
  })

  await EWE.start(addon);
  await EWE.filters.add(["||abptestpages.org/testfiles/blocking/full-path.png"]);
  // await EWE.subscriptions.add("https://easylist-downloads.adblockplus.org/v3/full/easylist.txt");
})();

chrome.runtime.onMessage.addListener(async function (message) {
  switch (message.type) {
    case 'getUserFilters':
      const currentUserFilters = await EWE.filters.getUserFilters();
      chrome.runtime.sendMessage({
        type: 'getUserFiltersResponse',
        currentUserFilters
      });
      break;
    case 'addFilter':
      await EWE.filters.add(message.filter);
      break;
    case 'removeFilter':
      await EWE.filters.remove(message.filter);
      break;
    default:
      break;
  }
})
