# wool

This is a wrapper around the webextension engine. It allows quick feedback on
using the webextension ad filtering solution.

## What is Wool?

- Standalone product, which can be released into the browser's extension web
  stores. This includes MV2 and MV3 extensions, in both Firefox and Chrome.
- It is a vessel for getting feedback on how it feels to use the engine in a full
  extension, including the process of getting it into the web stores.
- A fully functional ad blocker.
- Supports acceptable ads, and all of the other default subscriptions our engine recommends.
- Ships the engine in its default configuration.
- A reference implementation of an extension that uses our engine, which
  partners can use as documentation.
- An opportunity for us to experiment with new technologies and techniques.
- Wool uses the full range of engine functionality.
- An effort multiplier for our engine work. By providing feedback on how it is to
  actually use the engine, this will make us more effective in developing features
  and fixing bugs in the engine.

## What is Wool Not?

- A test extension. The engine should be self-testing, and Wool should assume that
  the engine works.
- A limited time experiment. This is an ongoing form of feedback and
  documentation.
- A separate team. This is developed and maintained by the same team as the
  webext engine.

## Development Practices and Standards

- This project uses trunk based development. There are NO merge requests, push
  your work straight to the `main` branch.
- We pair / mob on this project.
- We release frequently.

## To be determined

- How should we test Wool?
- Can we release this as a standalone eyeo product? How do we do this?

## Usage Notes

The extension manifest is defined in `src/manifest.js` and used by `@samrum/vite-plugin-web-extension` in the vite config.

Background, content scripts, options, and popup entry points exist in the `src/entries` directory. 

Content scripts are rendered by `src/entries/contentScript/renderContent.js` which renders content within a ShadowRoot
and handles style injection for HMR and build modes.

Otherwise, the project functions just like a regular Vite project.

To switch between Manifest V2 and Manifest V3 builds, use the MANIFEST_VERSION environment variable defined in `.env`

HMR during development in Manifest V3 requires Chromium version >= 110.0.5480.0.

Refer to [@samrum/vite-plugin-web-extension](https://github.com/samrum/vite-plugin-web-extension) for more usage notes.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### I just want to run this

```
npm run watch
```
and then
```
npm run serve:chrome
```

## Commands
### Build
#### Development, HMR

Hot Module Reloading is used to load changes inline without requiring extension rebuilds and extension/page reloads
Currently only works in Chromium based browsers.
```sh
npm run dev
```

#### Development, Watch

Rebuilds extension on file changes. Requires a reload of the extension (and page reload if using content scripts)
```sh
npm run watch
```

#### Production

Minifies and optimizes extension build
```sh
npm run build
```

### Load extension in browser

Loads the contents of the dist directory into the specified browser
```sh
npm run serve:chrome
```

```sh
npm run serve:firefox
```
